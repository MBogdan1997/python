class Coordinate(object):

    def __eq__(self, other):
        return self.x==other.x and self.y==other.y
    def __init__(self,x,y):
        self.x=x
        self.y=y
    def __str__(self):
        return "<"+str(self.x)+","+str(self.y)+">"
    def distance(self,other):
        x_diff_sq=(self.x-other.x)**2
        y_diff_sq=(self.y-other.y)**2
        return (x_diff_sq+y_diff_sq)**0.5
    @staticmethod
    def nimic():
        print("ceva")

c=Coordinate(3,4)
d=Coordinate(3,4)
print(c)
print(c.distance(d))
print(Coordinate.nimic())
print(c==d)

