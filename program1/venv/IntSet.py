class IntSet(object):

    def __init__(self):
        self.set=[]

    def insert(self,x):
        assert type(x)==int , "Lista de inturi"
        if self.member(x)==False:
            self.set.append(x)

    def member(self,x):
        for item in self.set:
            if item==x:
                return True
        return False

    def remove(self,x):

        for item in self.set:
            if item==x:
                self.set.remove(x)

    def __str__(self):
        self.set.sort()
        return '{' + ','.join(str(x) for x in self.set)+"}"

x=IntSet()
x.insert(3)
x.insert(1)
x.insert(4)
print(x)
x.remove(1)
print(x)
